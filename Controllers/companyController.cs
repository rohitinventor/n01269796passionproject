﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using jobshow.Models;
using System.Net;

namespace jobshow.Controllers
{
    public class companyController : Controller
    {
        private bind db = new bind();

        public ActionResult List()
        {
            return View(db.company.ToList());
        }


        //GET: Details of company
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            company com = db.company.Find(id);
            if (com == null)
            {
                return HttpNotFound();
            }
            return View(com);
        }

        // GET:  create company
        public ActionResult Create()
        {
            return View();
        }
        //POST: Save new company
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "companyId,companyName,companyownerName,companyType")] company comp)
        {
            if (ModelState.IsValid)
            {
                db.company.Add(comp);
                db.SaveChanges();
                return RedirectToAction("List");
            }
            return View(comp);
        }

        //GET: Edit company
        public ActionResult edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            company com = db.company.Find(id);
            if (com == null)
            {
                return HttpNotFound();
            }
            return View(com);
        }
        //POST: Edit company

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult edit([Bind(Include = "companyId,companyName,companyownerName,companyType")] company com)
        {
            if (ModelState.IsValid)
            {
                db.Entry(com).State = EntityState.Modified;
                db.SaveChanges();
              return RedirectToAction("List");
            }
            return View(com);
        }



        //GET: Delete of company
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            company com = db.company.Find(id);
            if (com == null)
            {
                return HttpNotFound();
            }
            return View(com);
        }
        //POST : delete
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            company com = db.company.Find(id);
            db.company.Remove(com);
            db.SaveChanges();
            return RedirectToAction("List");
            //return View();
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }



    }
}