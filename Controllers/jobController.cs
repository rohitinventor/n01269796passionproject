﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using jobshow.Models;
using System.Net;



namespace jobshow.Controllers
{
    public class jobController : Controller
    {
        private bind db = new bind();
        // GET: job
        public ActionResult List()
        {
            return View(db.job.ToList());
        }

        //GET: Details of job
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            job j = db.job.Find(id);
            if (j == null)
            {
                return HttpNotFound();
            }
            return View(j);
        }

        // GET:  create job
        public ActionResult Create()
        {
            return View();
        }
        //POST: Save new job
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "jobId,jobName,jobDetails,jobRequirement")] job j)
        {
            if (ModelState.IsValid)
            {
                db.job.Add(j);
                db.SaveChanges();
                return RedirectToAction("List");
            }
            return View(j);
        }

        //GET: Edit job
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            job j = db.job.Find(id);
            if (j == null)
            {
                return HttpNotFound();
            }
            return View(j);
        }
        //POST: Edit job

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "jobId,jobName,jobDetails,jobRequirement")] job j)
        {
            if (ModelState.IsValid)
            {
                db.Entry(j).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("List");
            }
            return View(j);
        }



        //GET: Delete of job
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            job j = db.job.Find(id);
            if (j == null)
            {
                return HttpNotFound();
            }
            return View(j);
        }
        //POST : delete
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            job j = db.job.Find(id);
            db.job.Remove(j);
            db.SaveChanges();
            return RedirectToAction("List");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }











    }
}