﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace jobshow.Models
{
    public class company
    {
        //companyid
        [Key]
        public int companyId { get; set; }

        //companyname
        public string companyName { get; set; }

        //owner
        public string companyownerName { get; set; }

        //company type
        public string companyType { get; set; }


    }
}