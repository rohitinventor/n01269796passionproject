﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace jobshow.Models
{
    public class job
    {
        //job id
        [Key]
        public int jobId { get; set; }

        //companyname
        public string jobName { get; set; }

        //owner
        public string jobDetails { get; set; }

        //company type
        public string jobRequirement { get; set; }

    }
}